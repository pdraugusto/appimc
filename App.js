import { StyleSheet, View, Text, StatusBar } from 'react-native';
import CabecalhoCard from './src/components/CardCab/CabecalhoCard';
import Button from './src/components/Button/Button';
import FormCard from './src/components/FormCard/FormCard';


export default function App() {
  return (
    <View style={styles.container}>

      <StatusBar/>

      <View style={styles.cabecalho}>
        
        <CabecalhoCard titulo='IMC' nome='Pedro Augusto de Arrascaeta'></CabecalhoCard>

      </View>

      <View style={styles.main}> 

        <FormCard />

      </View>

    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    // display: 'flex'
  },
  cabecalho: {
    // flex: 1,
    // backgroundColor: '#ff4',
    height: 200,
    minWidth: '100%'
  },
  main: {
    flex: 1,
    backgroundColor: 'aqua',
    minWidth: '100%',
    borderTopLeftRadius: 25,
    borderTopRightRadius: 25,
    padding: 35
  },
  footer: {
    // flex: 1,
    backgroundColor: '#000000',
    justifyContent: 'center',
    alignItems: 'center',
    minWidth: '100%',
    padding: 15
  }
});
