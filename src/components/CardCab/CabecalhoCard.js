import { Text, StyleSheet, Image, View, Dimensions } from "react-native";

import praia from "./img/praia.jpg"

const widthTela = Dimensions.get('screen').width;

const CabecalhoCard = ({titulo, nome}) => {

    return (
        <View style={style.cab}>
            <Image source={praia} style={style.image}></Image>
            <Text style={style.titulo}>{titulo}</Text>
            <Text style={style.desenv}>Desenvolvido por: {nome}
            </Text>
        </View>
    )

}

const style = StyleSheet.create({
    cab: {
        flex: 1,
        justifyContent: 'center',
        alignContent: 'flex-start'
    },
    desenv: {
        fontSize: 14,
        textAlign: 'right',
        color: 'black',
        position: 'absolute',
        bottom: 10,
        right: 10

    },
    image: {
        width: '100%',
        height: (4016 / 6016) * widthTela,
        position: 'absolute',
        top: 1
    },
    titulo: {
        fontSize: 60,
        fontWeight: 'bold',
        alignSelf: 'center',
        top: 0,
        padding: 25,
        position: 'absolute',
        color: 'black',
    }
})

export default CabecalhoCard