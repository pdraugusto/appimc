import { StyleSheet, Text, TouchableOpacity, View } from "react-native";

const Button = ( {titulo, click, corButton, corTexto} ) => {

    //console.log(props);
    //const {titulo} = props

    const style = StyleSheet.create({
        texto: {
            color: corTexto,
            textAlign: 'center'
        },
        botao: {
            marginTop: 20,
            backgroundColor: corButton,
            padding: 20,
            fontWeight: 'bold',
            borderRadius: 5
        }
    })

    return (
        <TouchableOpacity style={style.botao} onPress={click}>
            <Text style={style.texto}>{titulo}</Text>
        </TouchableOpacity>
    )

}

export default Button