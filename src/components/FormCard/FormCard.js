import { useState } from "react"
import { StyleSheet, Text, TextInput, TouchableOpacity, View } from "react-native"
import Button from "../Button/Button"


const FormCard = () => {

    const [usuario, setUsuario] = useState({
        nome: "",
        peso: 0,
        altura: 0,
        imc: 0
    })

    const calcular = () => {
        console.log(usuario);

        let imc = parseFloat(usuario.peso) / (parseFloat(usuario.altura) * parseFloat(usuario.altura))
        
        setUsuario({...usuario, imc: imc})
    }

    const back = () => {
        setUsuario({nome: "", peso: 0, altura: 0, imc: 0})
    }

    return(

        ((usuario.imc == 0) ?
            
            <View style={style.formContainer}>

                <View>
                    <Text style={style.formLabel}>Nome</Text>
                </View>
                <View style={style.formInput}>
                    <TextInput 
                        placeholder="Nome" 
                        value={usuario.nome}
                        onChangeText={(e) => setUsuario({...usuario, nome: e})}
                    />
                </View>
                <View>
                    <Text style={style.formLabel}>Altura (cm)</Text>
                </View>
                <View style={style.formInput}>
                    <TextInput 
                        placeholder="Altura"
                        value={usuario.altura}
                        onChangeText={(e) => setUsuario({...usuario, altura: e})}
                        keyboardType="decimal-pad"
                    />
                </View>
                <View>
                    <Text style={style.formLabel}>Peso (Kg)</Text>
                </View>
                <View style={style.formInput}>
                    <TextInput 
                        placeholder="Peso"
                        value={usuario.peso}
                        onChangeText={(e) => setUsuario({...usuario, peso: e})}
                        keyboardType="decimal-pad"
                    />
                </View>
                <View>
                    <Button 
                        titulo="Calcular IMC"
                        click={calcular}
                        corButton="#008"
                        corTexto="#fff"
                    />
                </View>

            </View>

            :

            <View>
                <Text>Olá {usuario.nome} seu IMC é de: {usuario.imc}</Text>
                <Button 
                    titulo="Voltar" 
                    corButton="#008"
                    click={back}
                    corTexto="#FFF" 
                />
            </View>

        )

        
    )

}

const style = StyleSheet.create({
    formContainer: {
        flex: 1,
        justifyContent: 'space-between',
        alignContent: 'flex-start'
    },
    formInput: {
        borderWidth: 3,
        padding: 15,
        fontSize: 22
    },
    formLabel: {
        fontSize: 22,
        fontWeight: 'bold',
        padding: 15
    }
})

export default FormCard